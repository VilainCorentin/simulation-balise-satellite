package item;

import java.awt.Point;
import Strategie.Move;
import visiteur.IVisitableDraw;
import visiteur.IVisitorDraw;

public class Entity implements IVisitableDraw{

	public Point pos;
	public Move moveStategie;
	public int id;
	public int speed;
	
	public Entity(int x, int y, int id) {
		this.pos = new Point(x, y);
		this.id = id;
		this.speed = 1;
	}
	
	public void move() {
		this.moveStategie.run(this);
	}

	@Override
	public void accept(IVisitorDraw IVisitor) {
	}
	
}
