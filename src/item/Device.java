package item;

abstract class Device extends Entity{

	public Boolean isEnabled;

	public Device(int x, int y, int id, Boolean isEnabled) {
		super(x, y, id);
		this.isEnabled = isEnabled;
	}

}
