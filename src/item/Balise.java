	package item;

import Evenements.MoveEvent;
import Evenements.OnSurfaceEvent;
import Strategie.HorizontalMove;
import Strategie.SynoMove;
import Strategie.VerticalMove;
import gestionEvent.Announcer;
import gestionEvent.Event;
import gestionEvent.ObserveurMethod;
import visiteur.IVisitableDraw;
import visiteur.IVisitorDraw;

public class Balise extends Device implements IVisitableDraw{

	public int rangeEmission;
	private ObserveurMethod observateurMethod;
	private ObserveurMethod ObserveurMethodSurface;
	
	public Balise(int x, int y, int id, Boolean isEnabled) {
		
		super(x, y, id, isEnabled);
		this.rangeEmission = 10;
		this.moveStategie = new VerticalMove(true);
		
		try {
			
			this.observateurMethod = new ObserveurMethod(this, this.getClass().getMethod("canSendData", Event.class));
			
			// Cr�ation du conteneur avec l'objet et la m�thode � appeler
			this.ObserveurMethodSurface = new ObserveurMethod(
												this,
												this.getClass().getMethod("onSurface", Event.class)
										  );
			
			// Inscription dans l'announcer avec notre conteneur et l'�v�nement qui doit d�clencher l'annonce
			// Ici, quand un objet atteint la surface
			Announcer.getInstance().register(this.ObserveurMethodSurface, OnSurfaceEvent.class);
		
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		}
	}
	
	public void move() {
		super.move();
	}
	
	public void onSurface(Event e) {
		// On r�cup�re l'objet qui a �mis l'evenement
		Entity ent = (Entity)e.getSource();
		
		// Si l'id correspond � la balise actuelle
		if(ent.id == this.id) {
			// Si elle �tait indisponible (car sous l'eau)
			if(!this.isEnabled) {
				// Elle s'inscrit sur un autre evenement
				Announcer.getInstance().register(this.observateurMethod, MoveEvent.class);
				// Elle se desinscrit de l'evenement OnSurfaceEvent (car elle sait qu'elle est en surface)
				Announcer.getInstance().unregister(this.ObserveurMethodSurface, OnSurfaceEvent.class);
				// Elle change de strat�gie de mouvement pour rester en surface
				this.moveStategie = new HorizontalMove(this.moveStategie.isDown);
				// Elle devient disponible pour synchronisation avec un satellite
				this.isEnabled = true;
			}
		}
	}
	
	
	public void canSendData(Event e) {
		Satellite sat = (Satellite)e.getSource();
		if(sat.isEnabled) {
			sat.isEnabled = false;
			
			if(sat.pos.getX() >= this.pos.getX() - this.rangeEmission && sat.pos.getX() <= this.pos.getX() + this.rangeEmission) {
				this.sendData();
			}
			
			sat.isEnabled = true;		
		}

	}
	
	private void sendData() {
		Announcer.getInstance().unregister(this.observateurMethod, MoveEvent.class);
		this.moveStategie = new SynoMove(this.moveStategie.isDown);
		super.move();
		this.isEnabled = false;
		Announcer.getInstance().register(this.ObserveurMethodSurface, OnSurfaceEvent.class);
	}

	@Override
	public void accept(IVisitorDraw IVisitor) {
		IVisitor.visitBalise(this);
	}

}
