package item;

import Evenements.MoveEvent;
import Strategie.HorizontalMove;
import gestionEvent.Announcer;
import visiteur.IVisitableDraw;
import visiteur.IVisitorDraw;

public class Satellite extends Device implements IVisitableDraw{

	public Satellite(int x, int y, int id, Boolean isEnabled) {
		super(x, y, id, isEnabled);
		this.moveStategie = new HorizontalMove(true);
		this.speed = 4;
	}

	public void move() {
		super.move();
		Announcer.getInstance().announce(new MoveEvent(this));
	}
	
	@Override
	public void accept(IVisitorDraw IVisitor) {
		IVisitor.visitSatelitte(this);
	}
	
}
