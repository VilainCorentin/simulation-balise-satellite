package Strategie;

import item.Entity;

public class Move implements IStrategie{

	final int MIN_MAP = 0;
	final int MAX_MAP_X = 800;
	final int MAX_MAP_BAL_Y = 300;
	final int MAX_MAP_SAT_Y = 600;
	
	public Boolean isDown;
	
	public Move(Boolean isDown) {
		this.isDown = isDown;
	}

	@Override
	public void run(Entity ent) {}
	
}
