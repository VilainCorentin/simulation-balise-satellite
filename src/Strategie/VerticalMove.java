package Strategie;

import Evenements.OnSurfaceEvent;
import gestionEvent.Announcer;
import item.Entity;

public class VerticalMove extends Move implements IStrategie{

	public VerticalMove(Boolean isDown) {
		super(isDown);
	}

	@Override
	public void run(Entity ent) {
		// Est-ce qu'on a atteint la surface ?
		if(ent.pos.getY() <= MAX_MAP_BAL_Y && this.isDown) {
			// Si oui, on retourne sous l'eau
			this.isDown = false;
			// On signale que l'on a atteint la surface
			Announcer.getInstance().announce(new OnSurfaceEvent(ent));
		}
		// Est-ce qu'on a atteint le fond ?
		else if(ent.pos.getY() >= MAX_MAP_SAT_Y && !this.isDown) {
			// Si oui, on remonte vers la surface
			this.isDown = true;
		}
		
		// Position en Y actuelle de l'entit�
		double moveDegre = ent.pos.getY();
		
		// Modification de la position pour remonter ou descendre
		if(this.isDown) {
			moveDegre--;
		}else {
			moveDegre++;
		}
		// Mise � jour de la position logique de l'entit�
		ent.pos.setLocation(ent.pos.getX(), moveDegre);
	}

}
