package Strategie;

import item.Entity;

public class HorizontalMove extends Move implements IStrategie{

	public HorizontalMove(Boolean isDown) {
		super(isDown);
	}

	@Override
	public void run(Entity ent) {
		if(ent.pos.getX() >= MAX_MAP_X) {
			ent.pos.setLocation(0,ent.pos.getY());
		}else {
			ent.pos.setLocation(ent.pos.getX() + ent.speed, ent.pos.getY());
		}
	}
	
}
