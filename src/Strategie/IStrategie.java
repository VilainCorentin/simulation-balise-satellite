package Strategie;

import item.Entity;

public interface IStrategie {
	void run(Entity ent);
}
