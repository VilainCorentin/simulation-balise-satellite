package Strategie;

import Evenements.OnSurfaceEvent;
import gestionEvent.Announcer;
import item.Entity;

public class SynoMove extends Move implements IStrategie {

	public SynoMove(Boolean isDown) {
		super(isDown);
	}

	@Override
	public void run(Entity ent) {
		this.vertical(ent);
		this.horizontal(ent);
	}

	private void vertical(Entity ent) {
		if (ent.pos.getY() <= MAX_MAP_BAL_Y && this.isDown) {
			this.isDown = false;
			Announcer.getInstance().announce(new OnSurfaceEvent(ent));
		} else if (ent.pos.getY() >= MAX_MAP_SAT_Y && !this.isDown) {
			this.isDown = true;
		}

		double moveDegre = ent.pos.getY();

		if (this.isDown) {
			moveDegre--;
		} else {
			moveDegre++;
		}
		ent.pos.setLocation(ent.pos.getX(), moveDegre);
	}

	private void horizontal(Entity ent) {
		if (ent.pos.getX() >= MAX_MAP_X) {
			ent.pos.setLocation(0, ent.pos.getY());
		} else {
			ent.pos.setLocation(ent.pos.getX() + ent.speed, ent.pos.getY());
		}
	}

}
