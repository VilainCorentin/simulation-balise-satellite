package visiteur;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Point;
import java.util.HashMap;

import graphicLayer.Oval;
import graphicLayer.RectMorph;
import graphicLayer.World;
import item.Balise;
import item.Entity;
import item.Satellite;

public class DrawVisiteur implements IVisitorDraw{

	World jc;
	HashMap<Integer, HashMap<Entity, Oval>> idToBalise;
	HashMap<Integer, HashMap<Entity, Oval>> idToSat;
	
	public DrawVisiteur() {
		this.idToBalise = new HashMap<>();
		this.idToSat = new HashMap<>();
		
		jc = new World("Simulation");
		jc.setBackground(Color.WHITE);
		jc.setPreferredSize(new Dimension(800, 600));
		
		jc.add(new RectMorph(Color.WHITE, new Point(0, 0), new Dimension(800, 300)));
		jc.add(new RectMorph(Color.BLUE, new Point(0, 300), new Dimension(800, 300)));
		
		jc.open();
	}
	
	@Override
	public void visitBalise(Balise b) {
		
		//Impl�mente la logique de la simulation sur l'objet
		b.move();
		
		//Cr�e le lien entre la structure de donn�es et le graphique
		if(!this.idToBalise.containsKey(b.id)) {
			Oval drawBal = new Oval(Color.RED, b.pos, new Dimension(10, 10));
			this.jc.add(drawBal);
			HashMap<Entity, Oval> mapDrawing = new HashMap<>();
			mapDrawing.put(b, drawBal);
			this.idToBalise.put(b.id, mapDrawing);
		}
		
		//Met � jour le graphique en cons�quence
		this.idToBalise.get(b.id).get(b).setPosition(b.pos);
	}

	@Override
	public void visitSatelitte(Satellite s) {
		
		s.move();
		
		if(!this.idToSat.containsKey(s.id)) {
			Oval drawBal = new Oval(Color.BLACK, s.pos, new Dimension(10, 10));
			this.jc.add(drawBal);
			HashMap<Entity, Oval> mapDrawing = new HashMap<>();
			mapDrawing.put(s, drawBal);
			this.idToSat.put(s.id, mapDrawing);
		}
		
		this.idToSat.get(s.id).get(s).setPosition(s.pos);
		
	}

}
