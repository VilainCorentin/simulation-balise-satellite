package visiteur;

import item.Balise;
import item.Satellite;

public interface IVisitorDraw {

	void visitBalise(Balise b);
	void visitSatelitte(Satellite s);
	
}
