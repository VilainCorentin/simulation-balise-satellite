package renderer;

import java.util.ArrayList;

import item.Balise;
import item.Entity;
import item.Satellite;
import visiteur.DrawVisiteur;

public class Simulation {

	public static void main(String[] args) {
		
		ArrayList<Entity> lstEntity = new ArrayList<>();
		
		int id = 0;
		
		DrawVisiteur visiteur = new DrawVisiteur();
		
		Balise bal = new Balise(100, 400, id, false);
		id++;
		
		lstEntity.add(bal);
		
		Balise bal2 = new Balise(250, 500, id, false);
		id++;
		
		lstEntity.add(bal2);
		
		Satellite sat = new Satellite(100, 100, id, true);
		id++;
		
		lstEntity.add(sat);
		
		Satellite sat2 = new Satellite(300, 150, id, true);
		id++;
		
		lstEntity.add(sat2);
		
		while(true) {
			for(int i = 0 ; i < lstEntity.size() ; i++) {
				lstEntity.get(i).accept(visiteur);;
			}
			try {
				Thread.sleep(20);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
	}

}
