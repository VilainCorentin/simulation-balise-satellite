package Evenements;

import gestionEvent.Event;

public class MoveEvent implements Event {

	Object source;

	public MoveEvent(Object source) {
		super();
		this.source = source;
	}

	@Override
	public void setSource(Object src) {
		this.source = src;
	}

	@Override
	public Object getSource() {
		return this.source;
	}

}
