package Evenements;

import gestionEvent.Event;

public class OnSurfaceEvent implements Event{
	
	Object source;
	
	public OnSurfaceEvent(Object source) {
		super();
		this.source = source;
	}

	@Override
	public void setSource(Object src) {
		this.source = src;
	}

	@Override
	public Object getSource() {
		return this.source;
	}
}
