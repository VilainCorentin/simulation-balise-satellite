package gestionEvent;

import java.lang.reflect.Method;

public class ObserveurMethod {

	private Object eventInstance;
	private Method method;

	public ObserveurMethod(Object eventClass, Method method) {
		super();
		this.eventInstance = eventClass;
		this.method = method;
	}

	public Object getEventInstance() {
		return eventInstance;
	}

	public void setEventInstance(Object eventInstance) {
		this.eventInstance = eventInstance;
	}

	public Method getMethod() {
		return method;
	}

	public void setMethod(Method method) {
		this.method = method;
	}

}
