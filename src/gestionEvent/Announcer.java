package gestionEvent;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Announcer {

	private Map<Class<? extends Event>, List<ObserveurMethod>> registrationIndex;

	private Announcer() {
		this.registrationIndex =  Collections.synchronizedMap(new HashMap<>());
	}
	
	public void register(ObserveurMethod o, Class<? extends Event> eventClass) {
		List<ObserveurMethod> l = registrationIndex.get(eventClass);
		if (l == null) {
			l = Collections.synchronizedList(new ArrayList<ObserveurMethod>());
			registrationIndex.put(eventClass, l );
		}
		l.add(o);
	}

	public void unregister (ObserveurMethod o, Class<? extends Event> eventClass) {
		List<ObserveurMethod> l = registrationIndex.get(eventClass);
		ObserveurMethod [] tmp = l.toArray(new ObserveurMethod [l.size()]);
		for (ObserveurMethod current : tmp) {
			if (o == current) l.remove(o);
		}
		if (l.isEmpty()) {
			registrationIndex.remove(eventClass);
		}
	}
	
	public void announce(Event anEvent) {
		Class<?> eventClass = anEvent.getClass();
		List<ObserveurMethod> l = registrationIndex.get(eventClass);
		if (l == null) return;
		ObserveurMethod [] tmp = l.toArray(new ObserveurMethod [l.size()]);
		for (ObserveurMethod current : tmp) {
			try {
				
				current.getMethod().invoke(current.getEventInstance(), anEvent);
				
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			}
		}
	}
	
	private static Announcer INSTANCE = new Announcer();
	
	public static Announcer getInstance() {
		return INSTANCE;
	}
	
}
