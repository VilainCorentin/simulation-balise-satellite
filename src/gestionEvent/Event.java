package gestionEvent;

public interface Event {
	void setSource(Object src);
	Object getSource();
}
