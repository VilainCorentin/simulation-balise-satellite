# Simulation Balise Satellite

School project that consists of performing a simulation of underwater tags collecting data with synchronization to satellites for sending data.

I invite you to read the report (in French) on the methods implemented for the project, in particular on the improvement of the code by using the adequate design patterns (strategy, visitor, ...).

[The report in PDF]

[The report in PDF]: <https://gitlab.com/VilainCorentin/simulation-balise-satellite/blob/master/Vilain%20Corentin%20-%20M2%20TIIL%20-%20Rapport%20technique.pdf>